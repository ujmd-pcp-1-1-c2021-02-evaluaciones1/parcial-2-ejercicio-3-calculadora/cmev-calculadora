﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Callculadora_PCP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            double Operando1 = Convert.ToDouble(txtOperando1.Text);
            double Operando2 = Convert.ToDouble(txtOperando2.Text);
            double Resultado = 0;

            if (rbtnSuma.Checked == true)
            {
                Resultado = Operando1 + Operando2;
            }
            if (rbtnResta.Checked) // forma abreviada del == true
            {
                Resultado = Operando1 - Operando2;
            }
            if (rbtnMultiplicación.Checked)
            {
                Resultado = Operando1 * Operando2;
            }
            if (rbtnDivisión.Checked)
            {
                Resultado = Operando1 / Operando2;
            }

            txtResultado.Text = Resultado.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void txtOperando1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtOperando2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
